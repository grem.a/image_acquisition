import os
import time
import cv2
import harvesters
import numpy as np
from genicam import gentl
from genicam.genapi import SingleChunkData
from genicam.gentl import (BUFFER_INFO_CMD_LIST, PAYLOADTYPE_INFO_IDS,
                           LibraryManager, TimeoutException)
from datetime import datetime
from harvesters.core import Harvester

class ImgAcq():
        
    __imgW_ = 2048
    __imgH_ = 1536
    
    ia = None
    h = None
    num_images = None
    
    def __init__(self):
        self.imgW_ = 2048
        self.imgH_ = 1536
    
    def getDeviceList(self):
        print(len(h.device_info_list))
    
    def setImageSize(self, imgW, imgH):
        self.__imgW_ = imgW
        self.__imgH_ = imgH

    def getImageW(self):
        return self.__imgW_

    def getImageH(self):
        return self.__imgH_    

    def camera_connect(self):
        scd = SingleChunkData()
        setChunk = True
        self.h = Harvester()
        self.h.add_file('/opt/cvb/drivers/genicam/libCVUSBTL.cti')
        self.h.update()
        self.ia = self.h.create_image_acquirer(0)            
        
        # configurations
        self.ia.remote_device.node_map.Width.value = self.__imgW_
        self.ia.remote_device.node_map.Height.value = self.__imgH_
        self.ia.remote_device.node_map.PixelFormat.value = 'Mono8'
        imshowWidth = 1080

        self.ia.keep_latest = False
        self.ia.num_filled_buffers_to_hold = 15

        # Set chunk mode active / enable chunk / get exposure time in data packet
        if setChunk:
            self.ia.remote_device.node_map.ChunkModeActive.value = True
            self.ia.remote_device.node_map.ChunkSelector.value = 'ExposureTime'
            self.ia.remote_device.node_map.ChunkEnable.value = True
            self.ia.remote_device.node_map.ChunkSelector.value = 'Gain'
            self.ia.remote_device.node_map.ChunkEnable.value = True
                
        # print("ChunkSelector: " + str(self.ia.remote_device.node_map.ChunkSelector.value))
        # print("ChunkEnable: " + str(self.ia.remote_device.node_map.ChunkEnable.value))
        # print("ChunkModeActive: " + str(self.ia.remote_device.node_map.ChunkModeActive.value))
    def destroy_ia(self):
        self.ia.stop_acquisition()
        self.ia.destroy()
        self.h.reset()

    def getImage(self):
        
        image = None    
        i = 1 
        self.num_images = 1

        while i > 0:                       
        
            self.ia.start_acquisition()            
            
            while self.num_images < i*100:  
            
                with self.ia.fetch_buffer() as buffer:       

                    component = buffer.payload.components[0]            
                    # Reshape and convert 2-dimensional to 3-dimensional array
                    print(str(component.height) + ' ' + str(component.width))
                    image = cv2.cvtColor(component.data.copy().reshape(component.height, component.width), cv2.COLOR_GRAY2RGB)

                    exposureTime = self.ia.remote_device.node_map.ChunkExposureTime.value
                    gain = self.ia.remote_device.node_map.Gain.value           
                        
                                                        
                    print(str(self.num_images) + ': ' + "exposure time: " + str(exposureTime)+ " / gain: " + str(gain))
                    cv2.imwrite('./ImgAcq/img/img'+str(self.num_images)+'.jpg', image)   
                    self.num_images += 1                         
            
            self.ia.stop_acquisition()
            self.ia.destroy()
            
            i += 1
            
            self.h.reset()
            ImgAcq.camera_connect(self)

# ObjImgAcq = ImgAcq()
# ObjImgAcq.camera_connect()
# ObjImgAcq.getImage()
