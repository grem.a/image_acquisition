import unittest
import sys
import numpy as np
import image_acq_tool as iat

class OpenCVTest(unittest.TestCase):
   
    def test_setWH_img_acq(self):        
        imgW = 88            
        imgH = 88
            
        iatObj = iat.ImgAcq()

        image = iatObj.getImage()
        imageH = iatObj.getImageH()
        imageW = iatObj.getImageW()

        self.assertEqual(2048, imageW, "equalW")
        self.assertEqual(1536, imageH, "equalH")

        iatObj.setImageSize(imgW, imgH)
        image1 = iatObj.getImage()
        realWxH = np.prod(image1.shape)/3
            
        self.assertEqual(imgW*imgH,realWxH, "equalWH")
 
if __name__ == '__main__':
    unittest.main()
